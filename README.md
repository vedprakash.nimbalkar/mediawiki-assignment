# Automate mediawiki Setup using helm chart.
Here are the steps to follow to install mediawiki application with mariadb database as backend.<br />

Clone the helm chart from <br />
`$git clone https://gitlab.com/vedprakash.nimbalkar/mediawiki-assignment.git`<br />

Move to the directory mediawiki-assignment<br />
`$cd mediawiki-assignment` <br />

Run helm install from helm-chart directory using path "/charts/mediawiki/". You may run helm install from any path, then you just need to provide full path of the directory where respective Chart.yaml is present.<br />
`$helm install mediawikiassignment /charts/mediawiki/ `<br />

Check the status once deployment starts.<br />
`$watch kubectl get all`<br />

`$ kubectl get all` <br />
`NAME                                      READY   STATUS             RESTARTS   AGE`<br />
`pod/mariadb-deployment-6559464b5f-q5mkv   1/1     Running            0          35m`<br />
`pod/mediawiki-665f6d4759-nkgmm            1/1     Running            0          38m`<br />
<br />
`NAME                        TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE`<br />
`service/kubernetes          ClusterIP      10.96.0.1        <none>        443/TCP          58m`<br />
`service/mariadb-service     ClusterIP      10.100.128.172   <none>        3306/TCP         38m`<br />
`service/mediawiki-service   LoadBalancer   10.104.1.94      10.104.1.94   8880:32090/TCP   38m`<br />
<br />
`NAME                                 READY   UP-TO-DATE   AVAILABLE   AGE`<br />
`deployment.apps/mariadb-deployment   1/1     1            1           38m`<br />
`deployment.apps/mediawiki            1/1     1            1           38m`<br />
<br />
`NAME                                            DESIRED   CURRENT   READY   AGE`<br />
`replicaset.apps/mariadb-deployment-6559464b5f   1         1         1       35m`<br />
`replicaset.apps/mediawiki-665f6d4759            1         1         1       38m`<br />
<br />

Wait for some time to get deploy the application. Once all services get deploy and ready to use. Check the List of deployments <br />
 `$ kubectl get deployment`<br />
 `NAME                 READY   UP-TO-DATE   AVAILABLE   AGE`<br />
 `mariadb-deployment   1/1     1            1           22m`<br />
 `mediawiki            1/1     1            1           22m`<br />

Use name "**deployment.apps/mediawiki**" to forward port from minikube to the local machine(local machine shoule have inetrnet access), to able to access URL from outside the environment.<br />

`$kubectl port-forward deployment.apps/mediawiki --address 0.0.0.0 8880:8880`<br />

Access the URL using `<Public ip/dns name>:8880`<br />

Note:- I used `minikube tunnel` in my minikube setup, to allow port forwarding and access from outside network.

**Veriables**

`database:`<br />
`root:`<br />
`    password: dGVzdDEyMw==           <- password for root user`<br />
`user:`<br />
`    password: dGVzdDEyMzQ=          <- password for databaseUser user`<br />
`databaseUser: wiki                  <- Username for databaseUser`<br />
`image:`<br />
`  db:`<br />
`    repository: vedprakashnimbalkar/mediawikidb       <- Repository of database Image`<br />
`    tag: latest                                       <- Version for database Image`<br />
`    pullPolicy: Always`<br />
`  app:`<br />
`    repository: vedprakashnimbalkar/mediawikiapp      <- Repository of mediawiki app Image`<br />
`    tag: 1.2                                          <- Verion for mediawiki app Image`<br />
`    pullPolicy: Always`<br />

Wiki setup need to complete after application deployment.<br />
Access the Application URL using `<DNS name of hosted server with port forwarding:8880 >  (EC2 instance Public DNS name in my case )` <br /> 
Steps required to follow are documentd here. [Post deploy Wiki setup](https://gitlab.com/vedprakash.nimbalkar/mediawiki-assignment/-/blob/master/preparation/MediaWiki_Setup.pdf)

**Limitations** <br />
- Web application is hardcoded to run on port 8880.
- Database is hard coadded to run on port 3306.
- This solution doesn’t have persistence volume configured.



